import './App.css';
import WordSearch from './components/WordSearch';

function App() {
  
  return (
    <div className="App">
      <header>
        <h1>Alphabet Soup</h1>
        <p>Welcome to Alphabet Soup, upload a file to start.</p>
        <p>File should contain board size, grid letters, and keywords.</p>
      </header>

      <WordSearch/>
    </div>
  );
}

export default App;
