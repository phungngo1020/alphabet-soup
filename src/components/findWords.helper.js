let directions = [[1,  0], [ 0,  1], [ 1, 1],
                  [1, -1],           [-1, 0],
                  [0, -1], [-1, -1], [-1, 1]];

// Search the matrix for matched words
export function findWords(grid, words) {
    let results = [];
    for (let x = 0; x < grid.length; x++) {
        for (let y = 0; y < grid[x].length; y++) {
            for (let word of words) {
                if (searchAround(grid, word, x, y).word)
                    results.push(searchAround(grid, word, x, y));
            }
        }
    }
    return results;
}

// Search all 8 directions from current index
export function searchAround(grid, word, x, y) {

    /** If character doesn't match word */
    if (grid[x][y] !== word[0]) {
        return false;
    }

    /** Search word in all 8 directions, starting from (x, y) */
    for (let i = 0; i < 8; i++) {
        let k, rd = x + directions[i][0], cd = y + directions[i][1];

        for (k = 1; k < word.length; k++) {
            /** Check out of bound */
            if (rd >= grid.length || rd < 0 || cd >= grid[0].length || cd < 0)
                break;
            /** If not matched */
            if (grid[rd][cd] !== word[k])
                break;

            if (k < word.length - 1) {
                rd += directions[i][0];
                cd += directions[i][1];
            }
        }
        if (k === word.length) {
            return { word: word, start: [x, y], end: [rd, cd] };
        }
    }
    return {};
}
