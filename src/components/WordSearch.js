import { useState } from 'react';
import { findWords } from './findWords.helper';
import uploadIcon from './upload-icon.png';

const WordSearch = () => {
    const [fileName, setFileName] = useState("");
    const [size, setSize] = useState(0);
    const [words, setWords] = useState([]);
    const [results, setResults] = useState([]);

    const readFile = (e) => {
        e.preventDefault();
        const reader = new FileReader();
        setFileName(e.target.files[0].name);
        reader.onload = (e) => {
            // Split file by line and store in a list
            let fullText = e.target.result.split('\n');

            // Filter out empty spaces
            fullText = fullText.filter(function (item) {
                return item !== '';
            })

            // Get grid length from first line
            let rowLength = fullText[0].split('x')[0];
            rowLength = parseInt(rowLength);
            setSize(fullText[0]);

            // Read words from end of list and store in words
            let wordsTmp = [];
            for (let i = rowLength + 1; i < fullText.length; i++) {
                wordsTmp.push(fullText[i]);
            }
            setWords(wordsTmp);

            // Store row arrays of grid
            let rowsTmp = [];
            for (let i = 1; i <= rowLength; i++) {
                rowsTmp.push(fullText[i].split(' '));
            }

            // Find position of words using helper function
            setResults(findWords(rowsTmp, wordsTmp));
        };
        reader.readAsText(e.target.files[0]);
    }

    return (
        <main>
            <section>
                <h2>UPLOAD FILE</h2>
                <div className="file-upload">
                    <label className="custom-file-upload">
                        <input type="file" onChange={(e) => readFile(e)} />
                        <img src={uploadIcon} alt="" width="60"/><br/>
                        <p>{fileName ? fileName : "Choose file..."}</p>
                    </label>
                    <hr/>
                    { size ? <p>Size: {size}</p> : "" }
                    { words.length ? <p>Words: {words.toString()}</p> : "" }
                </div>
            </section>

            <section>
                <h2>SOLUTIONS</h2>
                {results.length ?
                    <div className="solutions">
                        {results.map((res, i) => {
                            return (
                                <p key={i}>{res.word} {res.start[0]}:{res.start[0]} {res.end[0]}:{res.end[1]}</p>
                            )
                        })}
                    </div>
                    : <p>Upload a file to see solutions..</p>
                }
            </section>
        </main>
    )
}

export default WordSearch;