import { findWords, searchAround } from "./findWords.helper";

describe ("findWords() test", () => {
    let grid = [
        ['H','A','S', 'D', 'F'], 
        ['G','E','Y', 'B', 'H'], 
        ['J', 'K', 'L', 'Z', 'X'],
        ['C', 'V', 'B', 'L', 'N'],
        ['G', 'O', 'O', 'D', 'O']
    ];
    let words = ['HELLO', 'GOOD', 'BYE'];

    it("finds the matched word from grid", () => {
        let expected = [
            {word: "HELLO", start: [0,0], end: [4,4]},
            {word: "BYE", start: [1,3], end: [1,1]},
            {word: "GOOD", start: [4,0], end: [4,3]},
        ];
        
        expect(findWords(grid, words)[0]).toMatchObject(expected[0]);
        expect(findWords(grid, words)[1]).toMatchObject(expected[1]);
        expect(findWords(grid, words)[2]).toMatchObject(expected[2]);
    });
});

describe ("searchAround() test", () => {
    let grid = [
        ['H','A','S', 'D', 'F'], 
        ['G','E','Y', 'B', 'H'], 
        ['J', 'K', 'L', 'Z', 'X'],
        ['C', 'V', 'B', 'L', 'N'],
        ['G', 'O', 'O', 'D', 'O']
    ];
    let word = 'HELLO';
    let x = 0, y = 0;

    it("finds the matched word from grid", () => {
        let expected = {word: "HELLO", start: [0,0], end: [4,4]};
        
        expect(searchAround(grid, word, x, y)).toMatchObject(expected);
    });
});

